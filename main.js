let button = document.querySelector(".button");
let adressClient = document.querySelector(".adress-client");

class dataClientCard {
  constructor(continent, country, region, city, district) {
    this.continent = continent;
    this.country = country;
    this.region = region;
    this.city = city;
    this.district = district;
  }

  render() {
    let [...childAdressClient] = adressClient.children;

    if (childAdressClient.length > 0) {
      childAdressClient.forEach((elem) => {
        elem.remove();
      });

      let notice = document.createElement("h3");
      notice.className = "notice animation-notice";
      notice.innerText = "Result of new call done";
      adressClient.prepend(notice);
    }

    let cardUser = document.createElement("h3");
    cardUser.className = "item";
    cardUser.innerHTML = `Континент: ${this.continent} <br>
  Страна: ${this.country} <br>
  Регион: ${this.region} <br>
  Город: ${this.city} <br>
  Район: ${this.district} <br>`;
    adressClient.append(cardUser);
  }
}

async function findIp() {
  let apiAdress = await (await fetch("https://api.ipify.org/?format=json")).json();
  let adress = await (await fetch(`http://ip-api.com/json/${apiAdress.ip}?fields=continent,country,regionName,city,district`)).json();

  let { continent, country, regionName, city, district } = adress;
  let dataUser = [continent, country, regionName, city, district].map(
    (elem) => {
      if (elem == "") elem = "Неопределено";
      return elem;
    }
  );

  let dataClientIP = new dataClientCard(...dataUser);
  dataClientIP.render();
}

function addListenerButton() {
  button.addEventListener("click", () => {
    findIp();
  });
}

addListenerButton();
